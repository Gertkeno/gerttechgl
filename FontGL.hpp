#ifndef GERT_FONTWRAPPER_H
#define GERT_FONTWRAPPER_H

typedef struct _TTF_Font TTF_Font;
class SDL_Texture;
struct SDL_Rect;
struct SDL_Point;
class SDL_Window;

extern SDL_Window* gWindow;

typedef unsigned int GLuint;

#include <glm/glm.hpp>
#include <string>

namespace gert
{
	class Wavefront;

	class Font
	{
		private:
			static constexpr int _STORAGE_FONT{45};
			TTF_Font *_myFont;
			std::string _lastString[_STORAGE_FONT];
			int _textSize;
			uint8_t _activeStr;

			GLuint _lastTexture[_STORAGE_FONT];
		public:
			Font( void );
			~Font( void );
			bool load_font( const char* fontPath, int fontSize );
			bool load_font( void* dat, unsigned int datSize, int fontSize );
			bool set_active( std::string text );///< With texure cache this function isn't bad assuming there aren't more than STORAGE_FONT texts needed
			void draw( Wavefront* obj, std::string text, const glm::vec4& colorMod, const glm::vec2& pos, float scalar, float layer = 0.0f, float minHeight = 0.0f );
			//minHeight is a ratio of text.height/text.width

			/* void string_size( const char* text, float* rWidth, float* rHeight );///< with float pointers this returns a GL friendly w, h normalized to screen */
			float ratio( const char* text ) const;
			void string_size( const char* text, int* rWidth, int* rHeight ) const;///< with int pointers this returns SDL friendly based on pixels
	};
}

#endif // GERT_FONTWRAPPER_H
