#include <gert_GLmath.hpp>

#include <cmath>
#include <SDL2/SDL_shape.h>
#include <SDL2/SDL_video.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace gert
{
	glm::vec3 get_angles( const glm::vec3& from, const glm::vec3& to )
	{
		const float deltaX{ to.x - from.x };
		const float deltaY{ to.y - from.y };
		const float deltaZ{ to.z - from.z };
		const float hypot = sqrt( deltaX*deltaX + deltaY*deltaY + deltaZ*deltaZ );

		const glm::vec3 rpost{ -atan2( deltaX, deltaZ ), -asin( deltaY/hypot ), 0 };

		return rpost;
	}

	glm::vec3 point_to_gl_pos( const SDL_Point& pvec )
	{
		int screenWidth, screenHeight;
		SDL_GetWindowSize( gWindow, &screenWidth, &screenHeight );
		glm::vec3 foo;
		foo.x = ( static_cast<float>(pvec.x) / static_cast<float>(screenWidth) ) * 2 - 1;
		foo.y = ( static_cast<float>(pvec.y) / static_cast<float>(screenHeight) ) * 2 - 1;
		foo.y = foo.y * -1;
		foo.z = 0;

		return foo;
	}

	SDL_Point gl_pos_to_point( const glm::vec2& pos )
	{
		SDL_Point rme;
		int w, h;
		SDL_GetWindowSize( gWindow, &w, &h );

		rme = { static_cast<int>( ( pos.x + 1 )/2.0f * w ), static_cast<int>( ( pos.y - 1 )/-2.0f * h ) };
		return rme;
	}

	glm::mat4 rect_to_mat4( const SDL_Rect& pos )
	{
		int screenWidth, screenHeight;
		SDL_GetWindowSize( gWindow, &screenWidth, &screenHeight );
		int x, y;
		x = pos.x + pos.w/2;
		y = pos.y + pos.h/2;

		const glm::vec3 ctTransform{ point_to_gl_pos( { x, y } ) };

		glm::mat4 foo{ glm::translate( glm::mat4(), ctTransform ) };

		const float w{ pos.w / static_cast<float>(screenWidth) };
		const float h{ pos.h / static_cast<float>(screenHeight) };

		foo = glm::scale( foo, glm::vec3( w, h, 1.0f ) );

		return foo;
	}

	glm::vec3 normal_points( const glm::vec3& from, const glm::vec3& to )
	{
		glm::vec3 foo;
		foo.x = to.x - from.x;
		foo.y = to.y - from.y;
		foo.z = to.z - from.z;

		return glm::normalize( foo );
	}

	Quaternion make_quaternion( const glm::vec3& from, const glm::vec3& to )
	{
		//axis of rotation from standard position, origin, and target
		//rotation amount from arc length of y and z difference
		Quaternion dat = { { 0.0f, 0.0f, 1.0f }, 0.0f };
		if( from.z == to.z ) return dat;
		glm::vec3 a = glm::normalize( from );
		glm::vec3 b = glm::normalize( to );
		dat.axis = glm::cross( a, b );
		dat.rotate = sqrt( (a.length() * a.length()) * (b.length() * b.length()) ) + glm::dot( a, b );
		return dat;
	}

	bool fast_cstr_equal( const char* fo0, const char* fo1 )
	{
		for( auto i = 0u; fo0[i] !='\0' or fo1[i] != '\0'; i++ )
		{
			if( fo0[i] != fo1[i] ) return false;
		}
		return true;
	}
}
