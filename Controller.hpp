#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <map>
#include <vector>
#include <SDL2/SDL_events.h>

namespace gert
{
	struct Input
	{
		enum struct type_t: char{KEYBOARD, JOYBUTTON, JOYAXIS, MOUSE, NONE} myType;

		long int button;//SDL_keycode, button, mousebutton or axis id
		short axisMax; //just for axis, reciprocal
		bool axisPass; //passed framePress threshold

		float held;//0.0 to 1.0
		bool framePress;
	};

	typedef std::pair<size_t, Input> cunit;
	class Controller
	{
		public:
			Controller();
			~Controller();

			void manage_inputs( const SDL_Event* );

			float get_input ( size_t name ) const;
			float operator[]( size_t name ) const;//same as .get_input
			bool  get_press ( size_t name ) const;
			bool  operator()( size_t name ) const;//same as .get_press

			bool joy_add( int deviceIndex );
			bool joy_remove( SDL_JoystickID instanceID );
			int joy_count() const;

			bool load_file( const char * filename );
			bool save_file( const char * filename ) const;
			bool load_raw( const cunit * data, size_t );
			bool mutate_value( size_t, Input );
			size_t input_count() const;

			void reset_press();
			void reset_held();

			void debug_draw( void(*)( int, float ) );
		private:
			std::vector<Input> _data;
			std::vector<SDL_Joystick*> _joys;

			bool _tracked_joystick( SDL_JoystickID );
			template<typename Func>
			void _search_input( Input::type_t t, int id, Func&& x )
			{
				for( auto & i: _data )
				{
					if( i.myType != t ) continue;
					if( i.button != id ) continue;

					x( &i );
				}
			}
	};
}

#endif//CONTROLLER_H
