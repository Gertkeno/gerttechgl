#include <Controller.hpp>
#include <SDL2/SDL.h>
#include <fstream>
#include <algorithm>
#ifdef _GERT_DEBUG
#include <iostream>
#endif

using namespace gert;

namespace
{
	//normalized values
	constexpr float AXIS_PRESS_THRESHOLD{ 0.8f };
	constexpr float AXIS_RESET_THRESHOLD{ 0.7f };
	constexpr float AXIS_DEAD_ZONE{ 0.039f };

	void digital_press(Input* t)
	{
		if( t->held < 1.0f )
			t->framePress = true;
		t->held = 1.0f;
		//std::cerr << "Pressed button " << t->button << std::endl;
	}

	void digital_release(Input* t)
	{
		t->held = 0.0f;
	}
}

Controller::Controller() {}
Controller::~Controller() {}

void Controller::manage_inputs( const SDL_Event* e )
{
	auto stateDigital{ digital_release };
	//get press/release, if axis apply and return
	switch( e->type )
	{
		case SDL_JOYBUTTONDOWN:
			//only joysticks need to be tested, return if not tracked
			if( not _tracked_joystick( e->jbutton.which ) ) return;
		case SDL_KEYDOWN:
		case SDL_MOUSEBUTTONDOWN:
			stateDigital = digital_press;
			break;
		case SDL_JOYAXISMOTION:
			if( not _tracked_joystick( e->jaxis.which ) ) return;
			_search_input( Input::type_t::JOYAXIS, e->jaxis.axis, [e](Input* t)
			{
				t->held = static_cast<float>(e->jaxis.value) / t->axisMax;
				if( t->held < AXIS_DEAD_ZONE ) t->held = 0.0f;
				else if( t->held > 1.0f ) t->held = 1.0f;

				if( t->axisPass and t->held < AXIS_RESET_THRESHOLD )
					t->axisPass = false;
				else if( not t->axisPass and t->held > AXIS_PRESS_THRESHOLD )
				{
					t->framePress = true;
					t->axisPass = true;
				}
				//std::cerr << "Axis " << t->button << " value " << t->held << std::endl;
			});
			return;
	}

	//apply digital press/release
	switch( e->type )
	{
		case SDL_KEYUP:
		case SDL_KEYDOWN:
			_search_input( Input::type_t::KEYBOARD, e->key.keysym.sym, stateDigital );
			break;
		case SDL_MOUSEBUTTONUP:
		case SDL_MOUSEBUTTONDOWN:
			_search_input( Input::type_t::MOUSE, e->button.button, stateDigital );
			break;
		case SDL_JOYBUTTONUP:
		case SDL_JOYBUTTONDOWN:
			_search_input( Input::type_t::JOYBUTTON, e->jbutton.button, stateDigital );
			break;
	}
}

float Controller::get_input( size_t name ) const
{
	if( name > _data.size() )
	{
		#ifdef _GERT_DEBUG
		std::cerr << "[Controller]  Control held " << name << " too large\n";
		#endif
	}
	return _data[name].held;
}

float Controller::operator[]( size_t name ) const
{
	return get_input(name);
}

bool Controller::get_press( size_t name ) const
{
	if( name > _data.size() )
	{
		#ifdef _GERT_DEBUG
		std::cerr << "[Controller] Control press " << name << " too large\n";
		#endif
	}
	return _data[name].framePress;
}

bool Controller::operator()( size_t name ) const
{
	return get_press( name );
}

void Controller::reset_press()
{
	for( auto & i: _data )
		i.framePress = false;
}

void Controller::reset_held()
{
	for( auto & i: _data )
		i.held = 0.0f;
}

bool Controller::save_file( const char * filename ) const
{
	if( filename == nullptr ) return false;
	std::ofstream out( filename );
	if( not out.is_open() )
	{
		#ifdef _GERT_DEBUG
		std::cerr << "[Controller]  Couldn't open file " << filename << '\n';
		#endif
		return false;
	}

	out << _data.size();
	out << '\n';
	for( auto & i: _data )
	{
		out << static_cast<int>( i.myType );
		out << ' ';
		out << i.button;
		out << ' ';
		out << i.axisMax;

		out << '\n';
	}
	return true;
}

bool Controller::load_file( const char * filename )
{
	if( filename == nullptr ) return false;
	std::ifstream in( filename );
	if( not in.is_open() )
	{
		#ifdef _GERT_DEBUG
		std::cerr << "[Controller]  Couldn't open file " << filename << '\n';
		#endif
		return false;
	}

	decltype(_data.size()) index{0u};
	in >> index;
	in.get();
	while( index > 0 and not in.eof() )
	{
		int type;
		in >> type;
		decltype(Input::button) value;
		in >> value;
		decltype(Input::axisMax) axis;
		in >> axis;

		_data.push_back( {static_cast<Input::type_t>( type ), value, axis} );
		--index;
	}
	return true;
}

bool Controller::load_raw( const cunit * data, size_t s )
{
	_data.clear();

	_data.resize( s );
	for( auto a = 0u; a < s; ++a )
	{
		_data[data[a].first] = data[a].second;
	}
	return true;
}

bool Controller::mutate_value( size_t key, Input val )
{
	if( key > _data.size() )
	{
		#ifdef _GERT_DEBUG
		std::cerr << "[Controller]  Mutating " << key << " key too large\n";
		#endif
		return false;
	}
	_data[key] = val;
	reset_held();
	reset_press();
	return true;
}

bool Controller::_tracked_joystick( SDL_JoystickID foo )
{
	for( auto & i: _joys )
		if( SDL_JoystickInstanceID( i ) == foo ) return true;
	return false;
}

bool Controller::joy_add( int device_index )
{
	auto foo{ SDL_JoystickOpen( device_index ) };
	if( foo == nullptr ) return false;
	_joys.push_back( foo );
	#ifdef _GERT_DEBUG
	std::cerr << "[Controller]  Opened joystick " << device_index << '\t' << SDL_JoystickName( foo ) << '\n';
	#endif
	reset_held();
	return true;
}

bool Controller::joy_remove( SDL_JoystickID instance_id )
{
	for( auto i = 0u; i < _joys.size(); ++i )
	{
		if( instance_id != SDL_JoystickInstanceID( _joys[i] ) ) continue;
		#ifdef _GERT_DEBUG
		std::cerr << "[Controller]  Close joystick " << instance_id << '\t' << SDL_JoystickName( _joys[i] ) << '\n';
		#endif
		SDL_JoystickClose( _joys[i] );
		_joys.erase( _joys.begin()+i );
		reset_held();
		return true;
	}
	return false;
}

int Controller::joy_count() const
{
	return _joys.size();
}

void Controller::debug_draw( void(*d)(int, float) )
{
	size_t a{0u};
	for( auto & i : _data )
	{
		d( a++, i.held );
	}
}

size_t Controller::input_count() const
{
	return _data.size();
}
