#ifndef BUTTON_V2_H
#define BUTTON_V2_H

#include <string>
#include <SDL2/SDL_mouse.h>
#include <glm/glm.hpp>

namespace gert
{
	typedef decltype(SDL_BUTTON_LEFT) Mouse_Button;
	class Button
	{
		public:
			Button();
			~Button();

			void set( void (*draw)(Button*), void (*inc)(), void (*dec)() = nullptr );

			void draw();
			void update();

			std::string get_text() const;
			void set_text( std::string );

			glm::mat4 get_box() const;
			void set_box( glm::vec2 xy, glm::vec2 wh );

			bool get_inset() const;
			void force_inset();
			void force_inc() const;
			void force_dec() const;

			static int screenWidth;
			static int screenHeight;

			static int mx, my;
			static Mouse_Button mb;
		private:
			std::string _text;
			struct
			{
				void (*inc)(void);
				void (*dec)(void);
				void (*draw)(Button*);
			}_func;

			struct
			{
				glm::vec2 pos;
				glm::vec2 size;
			}_box;
	};
}

#endif //BUTTON_V2_H
