#ifndef GERT_GLCAMERA_H
#define GERT_GLCAMERA_H
#include <glm/detail/type_vec3.hpp>

namespace gert
{
struct Camera
{
	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 above;
};
}
#endif //GERT_GLCAMERA_H
