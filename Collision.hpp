#ifndef COLLISION_H
#define COLLISION_H

struct SDL_Rect;
struct SDL_Point;

#include <cmath>
#include <glm/detail/type_vec3.hpp>
#include <glm/detail/type_mat4x4.hpp>
///3D GL REVISION

struct Sphere
{
	float x, y, z, r;
	Sphere( glm::vec3 a ):
		x(a.x),y(a.y),z(a.z),r(0.0f){}
	Sphere( glm::vec3 a, float r_ ):
		x(a.x),y(a.y),z(a.z),r(r_){}
	constexpr Sphere( float x_, float y_, float z_, float r_ ):
		x(x_), y(y_), z(z_), r(r_){}
	constexpr Sphere():
		x(0.0f),y(0.0f),z(0.0f),r(0.0f){}

	glm::mat4 ct_mat( void ) const;
	glm::vec3 ct_vec3( void ) const { return {x, y, z}; }

	Sphere operator=( const glm::vec3& pos );
	Sphere operator+( const glm::vec3& pos );
	Sphere operator-( const glm::vec3& pos );
};

namespace collision
{

	//double distance( const float&, const float&, const float&, const float& );
	template<typename kind>
		double distance( const kind &x1, const kind &x2, const kind &y1, const kind &y2 )
		{
			kind distX = x2 - x1;
			kind distY = y2 - y1;
			return sqrt( ( distY * distY ) + ( distX * distX ) );
		}

	template<typename kinda, typename kindb>
		double distance( const kinda& t1, const kindb& t2 )
		{
			const double distX{ t1.x - t2.x };
			const double distY{ t1.y - t2.y };
			const double distZ{ t1.z - t2.z };
			return sqrt( ( distY * distY ) + ( distX * distX ) + ( distZ * distZ ) );
		}

	bool get_collide( const SDL_Rect& objA, const SDL_Rect& objB );
	//Circle collisions
	bool get_collide( const Sphere&, const Sphere& );
	bool get_collide( const Sphere& a, const SDL_Rect& box );

	void get_normal_diffXY( const SDL_Point& a, const SDL_Point& b, float* diffX, float* diffY );
}

#endif // COLLISION_H
