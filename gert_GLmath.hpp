#ifndef GERT_MATH_H
#define GERT_MATH_H

#include <glm/glm.hpp>
class SDL_Point;
class SDL_Rect;
class SDL_Window;
extern SDL_Window* gWindow;

namespace gert
{
	struct Quaternion
	{
		glm::vec3 axis;
		float rotate;
	};

	glm::vec3 get_angles( const glm::vec3& from, const glm::vec3& to );///< returns x, y, z ( 0, yaw, pitch ) as radians, rotate z (0,0,1) then rotate y (0,1,0)
	glm::vec3 point_to_gl_pos( const SDL_Point& pvec );
	SDL_Point gl_pos_to_point( const glm::vec2& pos );
	glm::mat4 rect_to_mat4( const SDL_Rect& pos );
	glm::vec3 normal_points( const glm::vec3& from, const glm::vec3& to );
	Quaternion make_quaternion( const glm::vec3& from, const glm::vec3& to );

	bool fast_cstr_equal( const char* fo0, const char* fo1 );
}
#endif // GERT_MATH_H
