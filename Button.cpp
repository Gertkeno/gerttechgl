#include <Button.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifdef _GERT_DEBUG
#include <iostream>
#endif //_GERT_DEBUG

using namespace gert;
static Button* _Inset{ nullptr };
static Mouse_Button _InType{ 0xFF };

int Button::mx{ 0 }, Button::my{ 0 };
Mouse_Button Button::mb{ 0xFF };

int Button::screenWidth{ 800 };
int Button::screenHeight{ 600 };

Button::Button()
{
	_func.inc = _func.dec = nullptr;
	_func.draw = nullptr;
}

Button::~Button()
{
}

//function setter
void Button::set( void (*draw)(Button*), void (*inc)(), void (*dec)() )
{
	_func.inc = inc;
	_func.dec = dec;
	_func.draw = draw;
}

void Button::draw()
{
	if( _func.draw != nullptr )
		_func.draw( this );
}

//inset outsiders
bool Button::get_inset() const
{
	if( _Inset == nullptr )
		return false;
	return this == _Inset;
}

void Button::force_inset()
{
	_Inset = this;
	_InType = 0xFF;
}

void Button::force_inc() const
{
	if( _func.inc != nullptr ) _func.inc();
}

void Button::force_dec() const
{
	if( _func.dec != nullptr ) _func.dec();
}

//string get/set
void Button::set_text( std::string text )
{
	_text = text;
}

std::string Button::get_text() const
{
	return _text;
}

//box get/set
glm::mat4 Button::get_box() const
{
	glm::mat4 rval = glm::translate( glm::mat4(), glm::vec3( _box.pos, 0.0f ) );
	rval = glm::scale( rval, glm::vec3( _box.size, 1.0f ) );
	return rval;
}

void Button::set_box( glm::vec2 xy, glm::vec2 wh )
{
	_box.pos = xy;
	_box.size = wh;
}

//event management
void Button::update()
{
	//collision checks
	bool collide = true;
	{
		int left = (_box.pos.x - _box.size.x + 1.0f) * screenWidth/2.0f;
		int righ = (_box.pos.x + _box.size.x + 1.0f) * screenWidth/2.0f;
		//invert y position, should be only SDL - GL conversion point
		int bot = (-_box.pos.y - _box.size.y + 1.0f) * screenHeight/2.0f;
		int top = (-_box.pos.y + _box.size.y + 1.0f) * screenHeight/2.0f;

		if( mx < left or mx > righ ) collide = false;
		if( my < bot or my > top ) collide = false;
	}

	//no collision down means no inset
	if( not collide )
	{
		if( get_inset() and _InType != 0xFF )
		{
			//std::cout << int(_InType) << '\n';
			_Inset = nullptr;
			_InType = 0xFF;
		}
		return;
	}

	if( mb == SDL_BUTTON_LEFT or mb == SDL_BUTTON_RIGHT )
	{
		_InType = mb;
		_Inset = this;
	}
	else if( get_inset() )
	{
		if( _InType == SDL_BUTTON_LEFT ) 
		{
			if( _func.inc != nullptr ) _func.inc();
		}
		else if( _InType == SDL_BUTTON_RIGHT ) 
		{
			if( _func.dec != nullptr ) _func.dec();
		}
		_Inset = nullptr;
		_InType = 0xFF;
	}
}
