#include "FontGL.hpp"

#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_shape.h>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>
#include <Shader.hpp>
#include <Wavefront.hpp>
#include <gert_GLmath.hpp>


//FONT WRAPPER
using namespace gert;
Font::Font( void )
{
	_myFont = nullptr;
	_activeStr = 0;
	for( auto &i: _lastTexture )
	{
		i = 0u;
	}
}

Font::~Font( void )
{
	glDeleteTextures( _STORAGE_FONT, _lastTexture );
	TTF_CloseFont( _myFont );
}

bool Font::load_font( const char* fontPath, int fontSize )
{
	if( _myFont != nullptr )
	{
		TTF_CloseFont( _myFont );
		_myFont = nullptr;
	}
	_activeStr = 0;
	_textSize = fontSize;
	_myFont = TTF_OpenFont( fontPath, fontSize );
	if( _myFont == NULL )
	{
		std::cerr << "Couldn't load font \n>" << TTF_GetError() << '\n';
		return false;
	}

	glGenTextures( _STORAGE_FONT, _lastTexture );
	return true;
}

bool Font::load_font( void* dat, unsigned int datSize, int fontSize )
{
	if( _myFont != nullptr )
	{
		TTF_CloseFont( _myFont );
		_myFont = nullptr;
	}

	_activeStr = 0;
	_textSize = fontSize;
	SDL_RWops* file = SDL_RWFromMem( dat, datSize );
	_myFont = TTF_OpenFontRW( file, true, fontSize );
	if( _myFont == nullptr )
	{
		std::cerr << "Couldn't load font\n>" << TTF_GetError() << '\n';
		return false;
	}

	glGenTextures( _STORAGE_FONT, _lastTexture );
	return true;
}

bool Font::set_active( std::string text )
{
	///check for existing textures
	int existing = -1;
	for( uint8_t i = 0; i < _STORAGE_FONT; i++ )
	{
		if( _lastString[ i ] != text ) continue;
		existing = i;
		//std::cerr << "[Font][set_active]  found text \"" << text << "\" at " << existing << '\n';
		break;
	}

	///actual rendering
	glActiveTexture( GL_TEXTURE0 );
	if( existing >= 0 )
	{
		glBindTexture( GL_TEXTURE_2D, _lastTexture[ existing ] );
	}
	else
	{
		//std::cerr << "[Font][set_active]  Couldn't find text \"" << text << "\" making texture with index " << _lastTexture[_activeStr] << '\n';
		glBindTexture( GL_TEXTURE_2D, _lastTexture[ _activeStr ] );
		_lastString[ _activeStr ] = text;
		SDL_Surface *tempMsg = TTF_RenderText_Blended( _myFont, text.c_str(), { 255, 255, 255, 255 } );
		if( tempMsg == NULL )
		{
			std::cerr << "Couldn't create text: " << text << " \n>" << TTF_GetError() << '\n';
		}
		else
		{
			int mode = GL_RGBA;
			if( tempMsg->format->BytesPerPixel == 3 )
			{
				mode = GL_RGB;
			}

			glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, tempMsg->w, tempMsg->h, 0, mode, GL_UNSIGNED_BYTE, tempMsg->pixels );
			SDL_FreeSurface( tempMsg );
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			++_activeStr;
			if( _activeStr >= _STORAGE_FONT )
			{
				_activeStr = 0;
			}
		}
	} // NOT THE LAST STRING

	if( Shader::Active_Shader != nullptr )
	{
		glUniform1i( glGetUniformLocation( Shader::Active_Shader->get_program(), "tex0" ), 0 );
		Shader::Active_Shader->set_uniform( "sheetWidth", 1u );
		Shader::Active_Shader->set_uniform( "sheetHeight", 1u );
	}
	return true;
}

void Font::draw( Wavefront* obj, std::string text, const glm::vec4& colorMod, const glm::vec2& pos, float scalar, float layer, float minHeight )
{
	int w, h;
	string_size( text.c_str(), &w, &h );

	//multi line text psuedo
	//new parameter "min-height"
	//if string-height/string-width is lower than min-height substr and recurse
	//else draw text

	float ratio = static_cast<float>(h)/w;
	if( ratio < minHeight )
	{
		auto wStrLimit = text.length();
		int sWidth = 0;
		int sHeight = 0;
		//remove letters until height/width ratio is minHeight
		while( wStrLimit > 1 and ratio < minHeight )
		{
			wStrLimit--;
			string_size( text.substr( 0, wStrLimit ).c_str(), &sWidth, &sHeight );
			ratio = static_cast<float>(sHeight)/sWidth;
		}

		//keep words together
		while( text[ wStrLimit-1 ] != ' ' and wStrLimit > 1 )
		{
			wStrLimit--;
		}

		if( wStrLimit > 1 )
		{
			std::string extraText = text.substr( wStrLimit );
			string_size( extraText.c_str(), &sWidth, nullptr );

			text = text.substr( 0, wStrLimit );
			string_size( text.c_str(), &w, &h );
			//match next line with current scale
			const float reScalar{ std::min( scalar, scalar * static_cast<float>(sWidth) / w ) };

			draw( obj, extraText, colorMod, { pos.x, pos.y - ratio*2*scalar }, reScalar, layer, minHeight );
			ratio = static_cast<float>(h)/w;
		}
	}

	set_active( text );

	glm::mat4 model = glm::translate( glm::mat4(), glm::vec3( pos.x, pos.y, layer ) );
	model = glm::scale( model, { scalar, ratio*scalar, 1.0f } );
	obj->draw( model, -1, colorMod );
}

/* void Font::string_size( const char* text, float* rWidth, float* rHeight ) */
/* { */
/* 	int totalWidth, totalHeight; */
/* 	SDL_GetWindowSize( gWindow, &totalWidth, &totalHeight ); */
/* 	int textWidth, textHeight; */
/* 	TTF_SizeText( _myFont, text, &textWidth, &textHeight ); */

/* 	totalWidth /= 2; */
/* 	totalHeight /= 2; */
/* 	if( rWidth != nullptr ) */
/* 		*rWidth = static_cast<float>(textWidth)/float(totalWidth); */
/* 	if( rHeight != nullptr ) */
/* 		*rHeight = static_cast<float>(textHeight)/float(totalHeight); */
/* } */

float Font::ratio( const char* text ) const
{
	int tw, th;
	TTF_SizeText( _myFont, text, &tw, &th );
	return static_cast<float>(th)/tw;
}

void Font::string_size( const char* text, int* rWidht, int* rHeight ) const
{
	TTF_SizeText( _myFont, text, rWidht, rHeight );
}
